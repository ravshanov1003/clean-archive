import os
import sys
import shutil
import zipfile
import tempfile
import logging

name = sys.argv[1]

with zipfile.ZipFile(f'{name}', "w") as zf:
    for dirpath, dirnames, files in os.walk("."):
        zf.write(dirpath)

with tempfile.TemporaryDirectory() as tempdir:
    zf.extractall(tempdir)

root = '.'
fileToSearch = '__init__.py'
arr = []
for relPath, dirs, files in os.walk(root):
    if ".git" in dirs:
        dirs.remove(".git")
    if ".tox" in dirs:
        dirs.remove(".tox")
    if "handlers" in dirs:
        dirs.remove("handlers")
    for dir in dirs:
        path = os.path.join(relPath, dir)
        arr.append(path)


cleaned = []
for i in arr:
    with os.scandir(i) as entries:
        for entry in entries:
            if entry.is_file():
                file = entry.name
                if file != fileToSearch:
                    cleaned.append(i)
                    shutil.rmtree(i)

cleaned.sort()
with open("cleaned.txt", "w") as fo:
    fo.writelines("%s\n" % i for i in cleaned)
logging.info('This is an info message')
# f = open('cleaned.txt', 'w')
# f.writelines("%s\n" % i for i in cleaned)
# f.close()

name = name[:-4]
with zipfile.ZipFile(f'{name}_new.zip', "w") as zf:
    for dirpath, dirnames, files in os.walk("."):
        zf.write('cleaned.txt')


# print(arr)
    # for file in files:
    #     if file != fileToSearch:
    #         path = os.path.join(root,relPath,file)
    #         print(path)

# f = []
# for (dirpath, dirnames, filenames) in os.walk(root):
#     path = os.path.join(dirpath,dirnames)
#     f.extend(path)
#     break
# print(f)

# with os.scandir() as entries:
#     for fold in entries:
#         if fold.is_dir():
#             print(fold.name)
